#!/usr/bin/env node

const assert = require("assert");

function one(n) {
  const b = "37".split("").map(Number);
  for (
    let i = 0, p = b.map((_, i) => i);
    b.length !== n + 10;
    i++, p = p.map(e => (e + b[e] + 1) % b.length)
  ) {
    b.push(
      ...String(p.reduce((a, e) => a + b[e], 0))
        .split("")
        .map(Number)
    );
  }
  return b.slice(n).join("");
}
assert.equal(one(360781), 6521571010);

function two(n) {
  const b = "37".split("").map(Number);
  for (
    let p = b.map((_, i) => i);
    !b
      .slice(b.length - n.length - 1)
      .join("")
      .includes(n);
    p = p.map(e => (e + b[e] + 1) % b.length)
  ) {
    b.push(
      ...String(p.reduce((a, e) => a + b[e], 0))
        .split("")
        .map(Number)
    );
  }
  return (
    b.length -
    n.length -
    1 +
    b
      .slice(b.length - n.length - 1)
      .join("")
      .indexOf(n)
  );
}
assert.equal(two("360781"), 20262967);
