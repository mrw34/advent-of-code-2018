#!/usr/bin/env node

const assert = require("assert");

const $ = require("fs")
  .readFileSync("24.txt")
  .toString()
  .trimRight()
  .split("\n");

function combat(boost = 0) {
  const groups = [];
  let army;
  for (const l of $) {
    if (!l.length) {
      continue;
    }
    if (l.endsWith(":")) {
      army = l.split(":")[0];
      continue;
    }
    const m = l.match(
      /(\d+) units each with (\d+) hit points (.*)with an attack that does (\d+) (.+) damage at initiative (\d+)/
    );
    const group = {
      army,
      units: Number(m[1]),
      health: Number(m[2]),
      immune: [],
      weak: [],
      damage: Number(m[4]) + (army === "Immune System" ? boost : 0),
      type: m[5],
      priority: Number(m[6]),
      get power() {
        return this.units * this.damage;
      }
    };
    if (m[3].trim().length) {
      for (let v of m[3]
        .trim()
        .replace(/[(),]/g, "")
        .split("; ")) {
        const w = v.split(" ");
        group[w[0]].push(...w.slice(2));
      }
    }
    groups.push(group);
  }

  while (
    !groups.filter(e => e.units > 0).every((e, i, a) => e.army === a[0].army)
  ) {
    groups.sort((a, b) => b.power - a.power || b.priority - a.priority);
    const targets = new Map();
    for (const attacker of groups) {
      if (attacker.units <= 0) {
        continue;
      }
      let target;
      for (const group of groups) {
        if (
          group.units <= 0 ||
          group.army === attacker.army ||
          [...targets.values()].includes(group) ||
          group.immune.includes(attacker.type)
        ) {
          continue;
        }
        if (!target) {
          target = group;
        }
        if (group.weak.includes(attacker.type)) {
          target = group;
          break;
        }
      }
      assert.ok(!targets.has(attacker));
      targets.set(attacker, target);
    }
    for (let attacker of [...targets.keys()].sort(
      (a, b) => b.priority - a.priority
    )) {
      if (attacker.units <= 0) {
        continue;
      }
      const target = targets.get(attacker);
      if (!target) {
        continue;
      }
      target.units -= Math.floor(
        (attacker.power * (target.weak.includes(attacker.type) ? 2 : 1)) /
          target.health
      );
    }
  }
  return groups.filter(e => e.units > 0);
}

assert.strictEqual(combat().reduce((a, e) => a + e.units, 0), 16006);
assert.strictEqual(combat(46).reduce((a, e) => a + e.units, 0), 6221);
