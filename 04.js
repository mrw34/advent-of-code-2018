#!/usr/bin/env node

const assert = require("assert");

const l = require("fs")
  .readFileSync("04.txt")
  .toString()
  .trimRight()
  .split("\n");
l.sort();
const m = l
  .map(e => e.match(/(\d+-\d+) .*:(\d+)] (.*)/).slice(1, 4))
  .map(e => [e[0], Number(e[1]), e[2]]);

const j = {};
{
  let g, h;
  for (const n of m) {
    if (n[2].includes("Guard")) {
      g = Number(n[2].split(" ")[1].split("#")[1]);
    } else {
      if (n[2].includes("falls")) {
        h = n[1];
      } else {
        for (let i = h; i < n[1]; i++) {
          if (!j[g]) {
            j[g] = {};
          }
          j[g][i] = (j[g][i] || 0) + 1;
        }
      }
    }
  }
}

function one() {
  const k = Object.entries(j).reduce(
    (a, [k, v]) =>
      Object.assign(a, { [k]: Object.values(v).reduce((a, e) => a + e) }),
    {}
  );
  const l = Object.keys(k).reduce((a, b) => (k[a] > k[b] ? a : b));
  const o = Object.keys(j[l]).reduce((a, b) => (j[l][a] > j[l][b] ? a : b));
  return l * o;
}

assert.equal(one(), 19830);

function two() {
  let x,
    y,
    z = 0;
  Object.entries(j).forEach(([k, v]) => {
    Object.entries(v).forEach(([k_, v_]) => {
      if (v_ > z) {
        z = v_;
        y = k;
        x = k_;
      }
    });
  });
  return y * x;
}

assert.equal(two(), 43695);
