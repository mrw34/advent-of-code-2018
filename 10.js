#!/usr/bin/env node

const assert = require("assert");

const l = require("fs")
  .readFileSync("10.txt")
  .toString()
  .trimRight()
  .split("\n")
  .map(e =>
    e
      .match(/<(.*),(.*)>.*<(.*),(.*)>/)
      .slice(1, 5)
      .map(Number)
  );

const d = l => {
  let s = "";
  const r = new Set();
  for (const p of l) {
    r.add(p.slice(0, 2).join());
  }
  const xs = l.map(p => p[0]);
  const ys = l.map(p => p[1]);
  for (let y = Math.min(...ys); y <= Math.max(...ys); y++) {
    for (let x = Math.min(...xs); x <= Math.max(...xs); x++) {
      s += r.has([x, y].join()) ? "#" : ".";
    }
    s += "\n";
  }
  return s;
};

const z = l => {
  for (let t = 1, s; ; t++) {
    const o = JSON.parse(JSON.stringify(l));
    for (const p of l) {
      p[0] += p[2];
      p[1] += p[3];
    }
    const xs = l.map(p => p[0]);
    const ys = l.map(p => p[1]);
    let n =
      Math.max(...xs) - Math.min(...xs) + Math.max(...ys) - Math.min(...ys);
    if (n > s) {
      return [o, t - 1];
    }
    s = n;
  }
};

function one() {
  return d(z(JSON.parse(JSON.stringify(l)))[0]);
}

assert.equal(
  one(),
  `#####...#....#..#........####...#####...#....#..######..#....#
#....#..#....#..#.......#....#..#....#..##...#..#.......#...#.
#....#..#....#..#.......#.......#....#..##...#..#.......#..#..
#....#..#....#..#.......#.......#....#..#.#..#..#.......#.#...
#####...######..#.......#.......#####...#.#..#..#####...##....
#.......#....#..#.......#..###..#..#....#..#.#..#.......##....
#.......#....#..#.......#....#..#...#...#..#.#..#.......#.#...
#.......#....#..#.......#....#..#...#...#...##..#.......#..#..
#.......#....#..#.......#...##..#....#..#...##..#.......#...#.
#.......#....#..######...###.#..#....#..#....#..#.......#....#
`
);

function two() {
  return z(JSON.parse(JSON.stringify(l)))[1];
}

assert.equal(two(), 10407);
