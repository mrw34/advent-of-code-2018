#!/usr/bin/env node

const assert = require("assert");

const l = require("fs")
  .readFileSync("06.txt")
  .toString()
  .trimRight()
  .split("\n")
  .map(e => e.split(", ").map(Number));

function one() {
  const [a, b] = [Math.min(...l.map(e => e[0])), Math.max(...l.map(e => e[0]))];
  const [c, d] = [Math.min(...l.map(e => e[1])), Math.max(...l.map(e => e[1]))];
  const f = new Map();
  const g = new Set();
  for (let x = a; x <= b; x++) {
    for (let y = c; y <= d; y++) {
      const m = new Map(
        l.map(e => [e, Math.abs(x - e[0]) + Math.abs(y - e[1])])
      );
      const j = Math.min(...m.values());
      const k = [...m.entries()].filter(([_, v]) => v === j);
      if (k.length === 1) {
        f.set([x, y], k[0][0]);
        if (x === a || x === b || y === c || y === d) {
          g.add(k[0][0]);
        }
      }
    }
  }
  const o = [...f.values()]
    .filter(e => !g.has(e))
    .reduce((a, e) => ({ ...a, [e]: (a[e] || 0) + 1 }), {});
  return Math.max(...Object.values(o));
}

assert.equal(one(), 3989);

function two(n) {
  const [a, b] = [Math.min(...l.map(e => e[0])), Math.max(...l.map(e => e[0]))];
  const [c, d] = [Math.min(...l.map(e => e[1])), Math.max(...l.map(e => e[1]))];
  let s = 0;
  for (let x = a; x <= b; x++) {
    for (let y = c; y <= d; y++) {
      const m = l.reduce(
        (a, e) => a + Math.abs(x - e[0]) + Math.abs(y - e[1]),
        0
      );
      if (m < n) {
        s++;
      }
    }
  }
  return s;
}

assert.equal(two(10000), 49715);
