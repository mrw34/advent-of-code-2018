#!/usr/bin/env node

const assert = require("assert");

const a = 7689;

const e = {};
const b = (x, y) => {
  const g = `${x},${y}`;
  let p = e[g];
  if (p !== undefined) {
    return p;
  }
  const i = x + 10;
  p = (i * y + a) * i;
  const v = "" + p;
  p = Number(v.charAt(v.length - 3)) - 5;
  e[g] = p;
  return p;
};

const q = zm => {
  const s = {};
  let m = 0,
    o;
  for (let z = 1; z <= zm; z++) {
    for (let x = 1; x <= 300 - z + 1; x++) {
      for (let y = 1; y <= 300 - z + 1; y++) {
        const g = `${x},${y}`;
        let n = s[g];
        if (n === undefined) {
          n = 0;
          for (let xz = x; xz < x + z; xz++) {
            for (let yz = y; yz < y + z; yz++) {
              n += b(xz, yz);
            }
          }
        } else {
          for (let xz = x; xz < x + z; xz++) {
            n += b(xz, y + z - 1);
          }
          for (let yz = y; yz < y + z - 1; yz++) {
            n += b(x + z - 1, yz);
          }
        }
        s[g] = n;
        if (n > m) {
          m = n;
          o = [x, y, z];
        }
      }
    }
  }
  return o;
};

assert.equal(
  q(3)
    .slice(0, 2)
    .join(),
  "20,37"
);

assert.equal(q(300).join(), "90,169,15");
