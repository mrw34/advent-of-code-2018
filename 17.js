#!/usr/bin/env node

const assert = require("assert");

const $ = require("fs")
  .readFileSync("17.txt")
  .toString()
  .trimRight()
  .split("\n");

const b = {};
let [x0, x1] = [Number.MAX_VALUE, 0];
let [y0, y1] = [Number.MAX_VALUE, 0];
for (const l of $.values()) {
  const p = l.split(/[=,. ]/);
  for (let i = Number(p[4]); i <= Number(p[6]); i++) {
    b[[p[0] === "x" ? p[1] : i, p[0] === "y" ? p[1] : i]] = "#";
  }
  [x0, x1] = [
    Math.min(x0, Number(p[0] === "x" ? p[1] : p[4])),
    Math.max(x1, Number(p[0] === "x" ? p[1] : p[6]))
  ];
  [y0, y1] = [
    Math.min(y0, Number(p[0] === "y" ? p[1] : p[4])),
    Math.max(y1, Number(p[0] === "y" ? p[1] : p[6]))
  ];
}
[x0, x1] = [x0 - 1, x1 + 1];
for (let y = y0; y <= y1; y++) {
  for (let x = x0; x <= x1; x++) {
    if (!b[[x, y]]) {
      b[[x, y]] = ".";
    }
  }
}

let z = [[500, y0]];
for (let [x, y] of z) {
  if (b[[x, y]] === "|") {
    continue;
  }
  if (b[[x, y]] === "." && !"~#".includes(b[[x, y + 1]])) {
    b[[x, y]] = "|";
    if (b[[x, y + 1]]) {
      z.push([x, y + 1]);
    }
    continue;
  }
  let xl;
  for (
    xl = x - 1;
    b[[xl, y]] && b[[xl, y]] !== "#" && "#~".includes(b[[xl, y + 1]]);
    xl--
  );
  let xr;
  for (
    xr = x + 1;
    b[[xr, y]] && b[[xr, y]] !== "#" && "#~".includes(b[[xr, y + 1]]);
    xr++
  );
  if (b[[xl, y]] === "#" && b[[xr, y]] === "#") {
    for (let x_ = xl + 1; x_ < xr; x_++) {
      b[[x_, y]] = "~";
    }
    b[[x, y - 1]] = ".";
    z.push([x, y - 1]);
  } else {
    for (let x_ = xl + 1; x_ < xr; x_++) {
      b[[x_, y]] = "|";
    }
    if (b[[xl, y]] !== "#") {
      z.push([xl, y]);
    }
    if (b[[xr, y]] !== "#") {
      z.push([xr, y]);
    }
  }
}

// const p = (x0, x1, y0, y1) => {
//   for (let y = y0; y <= y1; y++) {
//     for (let x = x0; x <= x1; x++) {
//       process.stdout.write(b[[x, y]]);
//     }
//     process.stdout.write("\n");
//   }
// };

assert.strictEqual(
  Object.values(b).filter(e => "~|".includes(e)).length,
  31412
);

assert.strictEqual(Object.values(b).filter(e => "~" === e).length, 25857);
