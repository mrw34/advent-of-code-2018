#!/usr/bin/env node

const assert = require("assert");

const l = require("fs")
  .readFileSync("02.txt")
  .toString()
  .split("\n");

function one() {
  return l
    .map(m =>
      m
        .split("")
        .reduce((a, e) => Object.assign(a, { [e]: (a[e] || 0) + 1 }), {})
    )
    .map(Object.values)
    .reduce(
      ([d, t], e) => [d + Number(e.includes(2)), t + Number(e.includes(3))],
      [0, 0]
    )
    .reduce((a, e) => a * e);
}

function two() {
  const c = new Set();
  for (const m of l) {
    for (const d of new Set(
      [...Array(m.length).keys()].map(
        (_, i) => m.substr(0, i) + m.substr(i + 1)
      )
    )) {
      if (c.has(d)) {
        return d;
      }
      c.add(d);
    }
  }
}

assert.equal(one(), 8296);
assert.equal(two(), "pazvmqbftrbeosiecxlghkwud");
