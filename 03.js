#!/usr/bin/env node

const assert = require("assert");

const l = require("fs")
  .readFileSync("03.txt")
  .toString()
  .trimRight()
  .split("\n")
  .map(e =>
    e
      .match(/(\d+) @ (\d+),(\d+): (\d+)+x(\d+)/)
      .slice(1, 6)
      .map(Number)
  );

const b = {};
for (const [i, x, y, w, h] of l) {
  for (let d = x; d < x + w; d++) {
    for (let e = y; e < y + h; e++) {
      const f = [d, e].join();
      b[f] = (b[f] || []).concat(i);
    }
  }
}

function one() {
  return Object.entries(b).filter(([k, v]) => v.length > 1).length;
}

assert.equal(one(), 115304);

function two() {
  for (const [i, x, y, w, h] of l) {
    let g = true;
    for (let d = x; d < x + w; d++) {
      for (let e = y; e < y + h; e++) {
        const f = [d, e].join();
        g = g && b[f].length === 1;
      }
    }
    if (g) {
      return i;
    }
  }
}

assert.equal(two(), 275);
