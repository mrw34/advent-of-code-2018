#!/usr/bin/env node

const assert = require("assert");

const l = require("fs")
  .readFileSync("01.txt")
  .toString()
  .split("\n")
  .map(Number);

function one() {
  return l.reduce((a, e) => a + e);
}

function two() {
  let p = 0;
  for (let i = 0, s = new Set(); ; i++) {
    p += l[i % l.length];
    if (s.has(p)) {
      break;
    }
    s.add(p);
  }
  return p;
}

assert.equal(one(), 553);
assert.equal(two(), 78724);
