#!/usr/bin/env node

const assert = require("assert");

const [p, m] = require("fs")
  .readFileSync("09.txt")
  .toString()
  .match(/(\d+) players; last marble is worth (\d+)/)
  .slice(1, 3)
  .map(Number);

const g = n => {
  const s = {};
  let d = { v: 0 };
  d.p = d.n = d;
  for (let v = 1; v < n + 1; v++) {
    if (v % 23 === 0) {
      d = d.p.p.p.p.p.p.p;
      d.p.n = d.n;
      d.n.p = d.p;
      s[v % p] = (s[v % p] || 0) + v + d.v;
      d = d.n;
    } else {
      const y = { v, p: d.n, n: d.n.n };
      d.n.n.p = y;
      d.n.n = y;
      d = y;
    }
  }
  return Math.max(...Object.values(s));
};

function one() {
  return g(m);
}

assert.equal(one(), 412959);

function two() {
  return g(m * 100);
}

assert.equal(two(), 3333662986);
