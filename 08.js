#!/usr/bin/env node

const assert = require("assert");

const l = require("fs")
  .readFileSync("08.txt")
  .toString()
  .trimRight()
  .split(" ")
  .map(Number);

const f = (l, r) => {
  if (l[0] === 0) {
    const m = l.slice(2, 2 + l[1]);
    return [m.reduce((a, e) => a + e), m.length + 2];
  }
  let t = [];
  let p = 2;
  for (let j = 0; j < l[0]; j++) {
    const q = f(l.slice(p), r);
    t.push(q[0]);
    p += q[1];
  }
  return [r(l.slice(p, p + l[1]), t), p + l[1]];
};

function one() {
  return f(l, (m, t) => m.concat(t).reduce((a, e) => a + e))[0];
}

assert.equal(one(), 49426);

function two() {
  return f(l, (m, t) =>
    m.reduce((a, e) => a + (t.length > e - 1 ? t[e - 1] : 0), 0)
  )[0];
}

assert.equal(two(), 40688);
