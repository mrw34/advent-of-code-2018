#!/usr/bin/env node

const assert = require("assert");

const a = require("fs")
  .readFileSync("13.txt")
  .toString()
  .split("\n")
  .map(e => e.split(""));

const b = [];

for (let y = 0; y < a.length; y++) {
  for (let x = 0; x < a[y].length; x++) {
    const e = { x, y, t: 0 };
    switch (a[y][x]) {
      case "^":
        b.push({ dx: 0, dy: -1, ...e });
        a[y][x] = "|";
        break;
      case ">":
        b.push({ dx: 1, dy: 0, ...e });
        a[y][x] = "-";
        break;
      case "v":
        b.push({ dx: 0, dy: 1, ...e });
        a[y][x] = "|";
        break;
      case "<":
        b.push({ dx: -1, dy: 0, ...e });
        a[y][x] = "-";
        break;
    }
  }
}

while (b.filter(e => !e.z).length > 1) {
  b.sort((e, f) => e.y - f.y || e.x - f.x);
  for (const c of b) {
    c.x += c.dx;
    c.y += c.dy;
    switch (a[c.y][c.x]) {
      case "/":
        if (c.dx === -1) {
          c.dx = 0;
          c.dy = 1;
        } else if (c.dx === 1) {
          c.dx = 0;
          c.dy = -1;
        } else if (c.dy === -1) {
          c.dx = 1;
          c.dy = 0;
        } else if (c.dy === 1) {
          c.dx = -1;
          c.dy = 0;
        }
        break;
      case "\\":
        if (c.dx === -1) {
          c.dx = 0;
          c.dy = -1;
        } else if (c.dx === 1) {
          c.dx = 0;
          c.dy = 1;
        } else if (c.dy === -1) {
          c.dx = -1;
          c.dy = 0;
        } else if (c.dy === 1) {
          c.dx = 1;
          c.dy = 0;
        }
        break;
      case "+":
        if (c.dx === -1) {
          c.dx = c.t === 0 ? 0 : c.t === 2 ? 0 : -1;
          c.dy = c.t === 0 ? 1 : c.t === 2 ? -1 : 0;
        } else if (c.dx === 1) {
          c.dx = c.t === 0 ? 0 : c.t === 2 ? 0 : 1;
          c.dy = c.t === 0 ? -1 : c.t === 2 ? 1 : 0;
        } else if (c.dy === -1) {
          c.dx = c.t === 0 ? -1 : c.t === 2 ? 1 : 0;
          c.dy = c.t === 0 ? 0 : c.t === 2 ? 0 : -1;
        } else if (c.dy === 1) {
          c.dx = c.t === 0 ? 1 : c.t === 2 ? -1 : 0;
          c.dy = c.t === 0 ? 0 : c.t === 2 ? 0 : 1;
        }
        c.t = (c.t + 1) % 3;
        break;
    }
    const d = b.filter(e => e.x === c.x && e.y === c.y && !e.z);
    if (d.length > 1) {
      console.log(c);
      d.forEach(e => (e.z = true));
    }
  }
}

console.log(b.find(e => !e.z));
