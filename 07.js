#!/usr/bin/env node

const assert = require("assert");

const l = require("fs")
  .readFileSync("07.txt")
  .toString()
  .trimRight()
  .split("\n")
  .map(e => e.match(/ ([A-Z]).* ([A-Z])/).slice(1, 3));

const z = (m, c) => {
  const o = new Set(m.reduce((a, e) => a.concat(e), []).filter(e => !c.includes(e)));
  for (const [x, y] of m) {
    if (!c.includes(x)) {
      o.delete(y);
    }
  }
  const p = [...o];
  p.sort();
  return p;
}

function one() {
  const c = [];
  let m = l.slice();
  while (m.length > 0) {
    c.push(z(m, c)[0]);
    m = m.filter(([_, y]) => !c.includes(y));
  }
  return c.join("");
}

assert.equal(one(), "OKBNLPHCSVWAIRDGUZEFMXYTJQ");

function two(n, d) {
  const c = [];
  let m = l.slice();
  const w = Array(n).fill(0).reduce((a, _, i) => ({[i + 1]: null, ...a}), {});
  for (let t = 0; m.length > 0; t++) {
    for (const [x, y] of Object.entries(w)) {
      if (y && y[1] < t) {
        c.push(y[0]);
        m = m.filter(([_, y]) => !c.includes(y));
        if (!m.length) {
          return t;
        }
        w[x] = null;
      }
    }
    const b = new Set(z(m, c));
    for (const [x, y] of Object.entries(w)) {
      if (y) {
        b.delete(y[0])
      }
    }
    if (!b.size) {
      continue;
    }
    for (const f of b.values()) {
      for (const [x, y] of Object.entries(w)) {
        if (!y) {
          w[x] = [f, t + "ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(f) + d];
          break;
        }
      }
    }
  }
}

assert.equal(two(5, 60), 982);
