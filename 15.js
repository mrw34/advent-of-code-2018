#!/usr/bin/env node

const $ = require("fs")
  .readFileSync("15.txt")
  .toString()
  .trimRight()
  .split("\n")
  .map(e => e.split(""));

// const q = 20;
const q = 3;

const a = ([x, y]) => {
  let d;
  for (const c of [[x, y - 1], [x - 1, y], [x + 1, y], [x, y + 1]].map(e =>
    e.join()
  )) {
    if (
      b.has(c) &&
      b.get(c).t === (b.get([x, y].join()).t === "G" ? "E" : "G")
    ) {
      if (!d || b.get(d).h > b.get(c).h) {
        d = c;
      }
    }
  }
  if (d) {
    b.get(d).h -= b.get([x, y].join()).t === "G" ? 3 : q;
    if (b.get(d).h <= 0) {
      b.set(d, { t: "." });
    }
    return true;
  }
};

const z = (c, t) => {
  const l = new Map([[c.join(), 1]]);
  for (const [e, d] of l) {
    const [x, y] = e.split(",").map(Number);
    for (const f of [[x, y - 1], [x - 1, y], [x + 1, y], [x, y + 1]]) {
      if (
        b.has(f.join()) &&
        [".", t === "G" ? "E" : "G"].includes(b.get(f.join()).t) &&
        !l.has(f.join())
      ) {
        l.set(f.join(), d + 1);
      }
    }
  }
  return l;
};

const m = c => {
  const [x, y] = c;
  const ss = [];
  for (let [k, v] of b) {
    if (v.t !== (b.get(c.join()).t === "G" ? "E" : "G")) {
      continue;
    }
    let s = [Number.MAX_SAFE_INTEGER];
    for (const f of [[x, y - 1], [x - 1, y], [x + 1, y], [x, y + 1]]) {
      if (!b.has(f.join()) || b.get(f.join()).t !== ".") {
        continue;
      }
      const l = z(f, b.get(c.join()).t);
      if (l.get(k) && (!s[1] || l.get(k) < s[0])) {
        s = [l.get(k), f];
      }
    }
    ss.push(s);
  }
  let t = [Number.MAX_SAFE_INTEGER];
  for (let s of ss) {
    if (!t[1] || s[0] < t[0]) {
      t = s;
    }
  }
  if (t[1]) {
    b.set(t[1].join(), b.get(c.join()));
    b.set(c.join(), { t: "." });
    return t[1];
  }
};

const p = () => {
  for (let [k, v] of b) {
    const [x, y] = k.split(",").map(Number);
    process.stdout.write(v.t);
    if (x === $[0].length - 1) {
      b.forEach(
        (v, k) =>
          k.split(",").map(Number)[1] === y &&
          v.h &&
          process.stdout.write(` ${v.h}`)
      );
      process.stdout.write("\n");
    }
  }
};

const b = new Map();
for (let y = 0; y < $.length; y++) {
  for (let x = 0; x < $[y].length; x++) {
    b.set(
      [x, y].join(),
      Object.assign({ t: $[y][x] }, "GE".includes($[y][x]) && { h: 200, i: -1 })
    );
  }
}

let i;
loop: for (i = 0; ; i++) {
  //   console.log(`${i}:`);
  //   p();
  for (let [k, v] of b) {
    if (!"GE".includes(v.t) || v.i === i) {
      continue;
    }
    if (new Set([...b.values()].map(e => e.t)).size < 4) {
      break loop;
    }
    let c = k.split(",").map(Number);
    if (a(c)) {
      continue;
    }
    c = m(c);
    if (c) {
      b.get(c.join()).i = i;
      a(c);
    }
  }
}

console.log(
  i *
    [...b.values()].filter(e => "GE".includes(e.t)).reduce((a, e) => a + e.h, 0)
);
