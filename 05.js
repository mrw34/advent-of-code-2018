#!/usr/bin/env node

const assert = require("assert");

const l = require("fs")
  .readFileSync("05.txt")
  .toString()
  .trimRight();

function f(n) {
  const m = n.split("");
  for (let a = 0; a < m.length - 1; ) {
    const [b, c] = m.slice(a, a + 2);
    if (b.toLowerCase() === c.toLowerCase() && b !== c) {
      m.splice(a, 2);
      a = Math.max(a - 1, 0);
      continue;
    }
    a++;
  }
  return m.length;
}

function one() {
  return f(l);
}

assert.equal(one(), 9562);

function two() {
  return Math.min(
    ..."abcdefghijklmnopqrstuvwxyz"
      .split("")
      .map(c => f(l.replace(new RegExp(c, "gi"), "")))
  );
}

assert.equal(two(), 4934);
