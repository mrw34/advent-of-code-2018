#!/usr/bin/env node

const assert = require("assert");

const $ = require("fs")
  .readFileSync("18.txt")
  .toString()
  .trimRight()
  .split("\n")
  .join("");

const n = (i, s) => {
  const l = Math.sqrt($.length);
  const c = { "#": 0, "|": 0, ".": 0 };
  let r = [];
  if (i % l === 0 && Math.floor(i / l) === 0) {
    //TL
    r = [1, l, l + 1];
  } else if (i % l === 0 && Math.floor(i / l) === l - 1) {
    //BL
    r = [-l, -l + 1, 1];
  } else if (i % l === 0) {
    //L
    r = [-l, -l + 1, 1, l, l + 1];
  } else if (i % l === l - 1 && Math.floor(i / l) === 0) {
    //TR
    r = [-1, l - 1, l];
  } else if (Math.floor(i / l) === 0) {
    //T
    r = [-1, 1, l - 1, l, l + 1];
  } else if (i % l === l - 1 && Math.floor(i / l) === l - 1) {
    //BR
    r = [-l - 2, -l, -1];
  } else if (i % l === l - 1) {
    //R
    r = [-l - 1, -l, -1, l - 1, l];
  } else if (Math.floor(i / l) === l - 1) {
    //B
    r = [-l - 1, -l, -l + 1, -1, 1];
  } else {
    r = [-l - 1, -l, -l + 1, -1, 1, l - 1, l, l + 1];
  }
  for (let j of r) {
    c[s[i + j]] += 1;
  }
  return c;
};

function f(m) {
  const p = {};
  let s = $;
  for (let i = 0; i < m; i++) {
    if (p[s]) {
      s = p[s];
      continue;
    }
    let b = "";
    for (let j = 0; j < s.length; j++) {
      const c = n(j, s);
      if (s[j] === "." && c["|"] >= 3) {
        b += "|";
      } else if (s[j] === "|" && c["#"] >= 3) {
        b += "#";
      } else if (s[j] === "#" && (c["#"] === 0 || c["|"] === 0)) {
        b += ".";
      } else {
        b += s[j];
      }
    }
    p[s] = b;
    s = b;
  }
  return (
    s.split("").filter(e => e === "|").length *
    s.split("").filter(e => e === "#").length
  );
}

assert.strictEqual(f(10), 620624);
assert.strictEqual(f(1000000000), 169234);
