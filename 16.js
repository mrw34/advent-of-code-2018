#!/usr/bin/env node

const assert = require("assert");

const $ = require("fs")
  .readFileSync("16.txt")
  .toString()
  .split("\n")
  .values();

const zz = [];
while (true) {
  const b = eval($.next().value.split("Before: ")[1]);
  if (!b) {
    break;
  }
  const i = $.next()
    .value.split(" ")
    .map(Number);
  const a = eval($.next().value.split("After: ")[1]);
  zz.push([b, i, a]);
  $.next();
}

const o = {
  addr(b, r) {
    r[b[2]] = r[b[0]] + r[b[1]];
  },
  addi(b, r) {
    r[b[2]] = r[b[0]] + b[1];
  },
  mulr(b, r) {
    r[b[2]] = r[b[0]] * r[b[1]];
  },
  muli(b, r) {
    r[b[2]] = r[b[0]] * b[1];
  },
  banr(b, r) {
    r[b[2]] = r[b[0]] & r[b[1]];
  },
  bani(b, r) {
    r[b[2]] = r[b[0]] & b[1];
  },
  borr(b, r) {
    r[b[2]] = r[b[0]] | r[b[1]];
  },
  bori(b, r) {
    r[b[2]] = r[b[0]] | b[1];
  },
  setr(b, r) {
    r[b[2]] = r[b[0]];
  },
  seti(b, r) {
    r[b[2]] = b[0];
  },
  gtir(b, r) {
    r[b[2]] = b[0] > r[b[1]] ? 1 : 0;
  },
  gtri(b, r) {
    r[b[2]] = r[b[0]] > b[1] ? 1 : 0;
  },
  gtrr(b, r) {
    r[b[2]] = r[b[0]] > r[b[1]] ? 1 : 0;
  },
  eqir(b, r) {
    r[b[2]] = b[0] === r[b[1]] ? 1 : 0;
  },
  eqri(b, r) {
    r[b[2]] = r[b[0]] === b[1] ? 1 : 0;
  },
  eqrr(b, r) {
    r[b[2]] = r[b[0]] === r[b[1]] ? 1 : 0;
  }
};

const u = z =>
  Object.entries(o).filter(([_, f]) => {
    const r = z[0].slice(0);
    f(z[1].slice(1), r);
    return r.join() === z[2].join();
  });

assert.strictEqual(zz.filter(z => u(z).length >= 3).length, 642);

const m = new Map();

while (Object.keys(o).length > 0) {
  for (const z of zz) {
    const p = u(z);
    if (p.length === 1) {
      m.set(z[1][0], p[0][1]);
      delete o[p[0][0]];
    }
  }
}

const r = [0, 0, 0, 0];

for (let l = $.next(); !l.done; l = $.next()) {
  const [h, ...t] = l.value.split(" ").map(Number);
  m.get(h)(t, r);
}

assert.strictEqual(r[0], 481);
