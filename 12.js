#!/usr/bin/env node

const assert = require("assert");

const a = require("fs")
  .readFileSync("12.txt")
  .toString()
  .trimRight()
  .split("\n");
const b = a[0].split(" ")[2];
const c = a
  .slice(2)
  .map(e => e.split(" "))
  .reduce((a, [f, _, t]) => ({ [f]: t, ...a }), {});
const j = 20;
let g = ".".repeat(j) + b + ".".repeat(j);
for (let e = 0; e < j; e++) {
  let f = "";
  for (let d = 0; d < g.length - 4; d++) {
    f += c[g.substring(d, d + 5)] || ".";
  }
  g = ".." + f + "..";
}
const k = g.split("").reduce((a, e, i) => (a += e === "#" ? i - j : 0), 0);
assert.equal(k, 1184);
